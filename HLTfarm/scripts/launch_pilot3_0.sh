#
# script to launch the pilot on the HLTfarm
#

#  need to force the USER
export USER=lhcbprod
# force the location of the USER
export HOME=/home/lhcbprod
ulimit -u 5120
ps afx | grep diracmon | grep -v grep
if [ $? -eq 1 ] ; then
  echo exit
fi

WORKDIR=false
#
#  test which localdisk we should use
#
if [ -r /localdisk1/dirac ]; then
  WORK_DIR=/localdisk1/dirac/work
  if [ ! -d "$WORK_DIR" ]; then
    mkdir -p $WORK_DIR
    if [ ! $? -eq 0 ]; then
      echo "Work dir '"$WORK_DIR"' could not be created"
    else
      WORKDIR=true
    fi
  else
    WORKDIR=true
  fi
fi

if [ $WORKDIR == false ]; then
  if [ -r /localdisk2/dirac ] ; then
    WORK_DIR=/localdisk2/dirac/work
    if [ ! -d "$WORK_DIR" ]; then
      mkdir -p $WORK_DIR
      if [ ! $? -eq 0 ]; then
        echo "Work dir '"$WORK_DIR"' could not be created"
        exit 1
      fi
    fi
  else
    echo "No localdisk mounted"
    exit 1
  fi
fi
echo $WORK_DIR


# set the LHCb environment
source /cvmfs/lhcb.cern.ch/lib/LbLogin.sh

#  set the certificate directories
export http_proxy=http://netgw01:8080
export X509_CERT_DIR=/cvmfs/lhcb.cern.ch/etc/grid-security/certificates/
export X509_VOMS_DIR=/cvmfs/lhcb.cern.ch/etc/grid-security/vomsdir

# check if the working directory exist
if ! [ -d "$WORK_DIR" ] ; then
  exit 1
fi
cd $WORK_DIR
if [ -d "$WORK_DIR/lhcb" ] ; then
  rm -rf $WORK_DIR/lhcb/*
fi

echo "Here I am " $PWD
echo "User: "$USER

$HOME/production/dirac-pilot-3.sh --dirac-tmp=$WORK_DIR
