""" Unit tests for the LbMesosAPI"""

import unittest
import json
import mock
import requests
from lbmesos.marathon.marathonAPI import MarathonAPI
#pylint: disable=invalid-name

def RequestMagicMock(status_code, jsonContent):
  """ Creates a mock of request that can be reused.
      It implements the basic interface of requests.response
  """
  def mk_raise_on_status():
    """ Mock of raise_on_status"""
    if status_code != 200:
      raise requests.HTTPError("RequestMagicMock raise on statu")

  return mock.MagicMock(
      spec = requests.Response,
      status_code = status_code,
      response = json.dumps(jsonContent),
      json = mock.MagicMock(return_value = jsonContent),
      raise_for_status = mk_raise_on_status)


@mock.patch('lbmesos.Utils.request')
class test_findMarathonLeader(unittest.TestCase):
  """ Tests MarathonAPI.findMarathonLeader
  """

  def test_all_ok(self, mk_req):
    """ Here it should all work nicely, and the leader should be first in the list"""

    # First, everything is fine
    mk_req.return_value = RequestMagicMock(200,{'leader': 'server1'} )

    api = MarathonAPI(['http://server3', 'http://server1', 'http://server2'])

    # The leader is 1
    self.assertEqual(api.marathonLeader, 'http://server1')
    # The leader should be in front
    self.assertListEqual(api.marathonMasterList, ['http://server1', 'http://server3', 'http://server2'])

  def test_all_ok_not_in_list(self, mk_req):
    """ Here it should all work nicely,
        but the leader is not in the list
    """

    # First, everything is fine
    mk_req.return_value = RequestMagicMock(200,{'leader': 'server1'} )


    api = MarathonAPI(['http://server3', 'http://server2'])

    self.assertEqual(api.marathonLeader, 'http://server1')
    self.assertListEqual(api.marathonMasterList, ['http://server3', 'http://server2'])

  def test_no_leader(self, mk_req):
    """ No leader is defined
    """

    # First, everything is fine
    mk_req.return_value = RequestMagicMock(200,{'leader': ''} )

    with self.assertRaises(ValueError):
      MarathonAPI(['http://server3', 'http://server2'])

  def test_bad_status_code(self, mk_req):
    """ Does not return 200
    """

    # First, everything is fine
    mk_req.return_value = RequestMagicMock(400,{'leader': 'server1'} )

    with self.assertRaises(requests.HTTPError):
      MarathonAPI(['server3', 'server2'])
