""" Unit tests for the LbMesosAPI"""

import unittest
import json

import mock
import requests
from lbmesos.Utils import request, request_with_raise

#pylint: disable=invalid-name


def RequestMagicMock(status_code, jsonContent):
  """ Creates a mock of request that can be reused.
      It implements the basic interface of requests.response
  """

  def mk_raise_on_status():
    """ Mock of raise_on_status"""
    if status_code != 200:
      raise requests.HTTPError("RequestMagicMock raise on status")

  return mock.MagicMock(
      spec = requests.Response,
      status_code = status_code,
      response = json.dumps(jsonContent),
      json = mock.MagicMock(return_value = jsonContent),
      raise_for_status = mk_raise_on_status)


# REMINDER: decorator go in the opposit order as attributes !
@mock.patch('requests.request')
class test_request(unittest.TestCase):
  """ Tests Utils.request
  """

  def test_request_retry_works_eventually(self, mk_request):
    """ Test the request wrapper"""

    # We should have two servers
    # the first one tries to connect, the second one works
    mk_request.side_effect = [requests.ConnectionError('Test error'), None]
    response = request('post', ['http://shouldFail', 'http://shouldWork'], 'relativeURL')

    self.assertEqual(response, None)

  def test_request_retry_fails(self, mk_request):
    """ Check that we get an exception when nothing works"""

    # We should get an exception since none will work
    mk_request.side_effect = requests.ConnectionError('Test error')

    with self.assertRaises(requests.ConnectionError):
      request('post',['http://shouldFail', 'http://shouldAlsoFail'], 'relativeURL')

  def test_bad_response(self, mk_request):
    """ Test than when getting a non OK status, we handle it corectly"""

    mk_request.return_value = RequestMagicMock(400, {'leader': ''})

    # Using request, we should just get the status_code 400
    response = request('post', ['http://shouldWork'], 'relativeURL')
    self.assertEqual(response.status_code, 400)

    # Using request_with_raise, we should get an exception requests.HTTPError
    with self.assertRaises(requests.HTTPError):
      request_with_raise('post', ['http://shouldWork'], 'relativeURL')
