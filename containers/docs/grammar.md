# Configuration grammar

This documentation explains the grammar used to configure the Mesos cluster.

First of all, the configuration is a json file.

## Mandatory field

In order to be parsed by the tools, a few fields are mandatory:

* `grammarVersion`: This is a just a tag in case the grammar needs to change, and compatibility preserved. Currently, only "Alba" exists. (numbers are boring for versionning...)
* `Tasks`: this is a section that needs to be filed as described bellow

## Tasks section

Under this section, all the tasks for each frameworks are defined.
It is devided per framework.

```
"Tasks": {
  "Framework1" : {
    "Task1": {
      "attr1" : 1
    },
    "Task2": {
      "attr1" : 3
    }
  },
  "Framework2" : {
    "Task1": {
      "attr1" : 1
    }
  }
}
```
## Definition of a task

Each task has at least one mandatory parameter: `template`.
This points to a file which contains the task definition as it will be sent to the framework.
It can either be complete, or with variables (see bellow).

Only the leave of the JSON sections are transformed into tasks.

The name of the task is the concatenation of the section from Tasks till the end. Intermediate node can be ignored if they are prepended with a `_`.



### Factorization

In order to make the configuration easier, the grammar allows you to factorize some attributes. Hense, whatever attribute is defined under Framework1 is available in Task1 and Task2 of this framework. For example

```
      "_LHCbDIRAC": {
          "instances": 1,
          "template": "toto",
          "DataManagement": {
             "instances": 2,
             "FileCatalog": {
                  "port": 9197
             },
             "Bookkeeping": {
                  "instances": 3,
                  "port": 9198
             }
          }
      }
```

gives the followint two tasks

```
[(u'_LHCbDIRAC/DataManagement/Bookkeeping/',
  {u'instances': 3, u'port': 9198, u'template': u'toto'}),
 (u'_LHCbDIRAC/DataManagement/FileCatalog/',
  {u'instances': 2, u'port': 9197, u'template': u'toto'})]
```

### Template definition

The template files are text files which are read, interpreted as python string, and the variables replaced.
The variables available in a template files are all the attributes defined in the JSON task definition, but in capital letters.

For example, if you define a task this way:

```
"Task1": {
   "instances": 1,
   "port": 123
}
```

You could use them this way in the template:

```
{
    "fixed": "/whatever",
    "instances": %(INSTANCES)s,
    "port": %(PORT)s
}
```

Two special variables are created automatically:
* `SERVICE_ID`: this contains the task name, without the node prepended by an underscore
* `NODE_NAME`: The last part of the task name, separated with slash
