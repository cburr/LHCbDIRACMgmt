import urllib2
import json
import urlparse
import os
import CfgParser

def CfgManagement(configFileName):
  """ Return a Parser compatible with the version grammar"""
  # If it is a local file, we resolve it and add the file protocol
  parsed = urlparse.urlsplit(configFileName)
  if not parsed.scheme:
    parsed = parsed._replace(path=os.path.realpath(configFileName))
    parsed = parsed._replace(scheme = 'file')
    configFileName = urlparse.urlunsplit(parsed)
  config = json.loads(urllib2.urlopen(configFileName).read())
  _grammarVersion=config['grammarVersion']
  return CfgParser.CfgParser(configFileName, config)
