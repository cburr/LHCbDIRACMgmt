""" Collection of utilities"""

import logging
import urlparse
import requests
import cookielib
import os

import subprocess


def get_sso_cookie(serverUrl):
  """ Returns the sso cookie in a format understood
      by requests, only if https. Generate it if it does not exist yet
      (requires cern-get-sso-cookie)
  """
  parsedUrl = urlparse.urlparse(serverUrl)
  if parsedUrl.scheme != 'https':
    return None
  netloc = parsedUrl.netloc
  cookieFile = '/tmp/%s.sso' % netloc

  # If the cookie file does not exist, generate it
  if not os.path.exists(cookieFile):
    errCode = subprocess.call(['cern-get-sso-cookie', '--krb', '-r', '-u', serverUrl, '-o', cookieFile])
    if errCode:
      raise requests.ConnectionError("Could not get sso for %s: %s" % (serverUrl, errCode))


  cj = cookielib.MozillaCookieJar(cookieFile)
  cj.load()
  return cj


def request(method, serverList, relativeURL, *args, **kwargs):
  """ Wrapper to requests.request but with URL retry.
      See http://docs.python-requests.org/en/master/api/#requests.request

      :param method: HTTP method to use
      :param serverList: list of base url of the servers
      :param relativeURL: url to append to the list of servers (e.g. /v2/ping)

      Note that we set verify to False because the cern ca is not trusted by default

      :returns: requests.Response object
  """

  maxAttempts = len(serverList)

  attempt = 0
  while attempt < maxAttempts:
    candidateServer = serverList[attempt]
    attempt += 1
    candidateURL = urlparse.urljoin(candidateServer, relativeURL)
    try:
      if 'cookies' not in kwargs:
        candidateCookie = get_sso_cookie(candidateServer)
        kwargs['cookies'] = candidateCookie
      if 'verify' not in kwargs:
        kwargs['verify'] = False

      res = requests.request(method, candidateURL, *args, **kwargs)
      return res
    except (requests.ConnectionError, requests.Timeout) as ex:
      logging.warning("Got exception when trying %s: %s", candidateURL, repr(ex))

  raise requests.ConnectionError("None of the candidate is answering to requests")


def request_with_raise(*args, **kwargs):
  """ Forwards the call to self.request
      and raise in case return status != 200
  """
  response = request(*args, **kwargs)
  response.raise_for_status()
  return response
