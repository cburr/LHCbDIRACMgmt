"""
This script can be used to reindex data from a source cluster tp the destination cluster.
"""
from elasticsearch import Elasticsearch
from elasticsearch.helpers import BulkIndexError, bulk, reindex

import multiprocessing

mapping = {'WMSHistory': {'properties': {'ApplicationStatus': {'type': 'keyword'},
   'JobGroup': {'type': 'keyword'},
   'JobSplitType': {'type': 'keyword'},
   'MinorStatus': {'type': 'keyword'},
   'Site': {'type': 'keyword'},
   'Status': {'type': 'keyword'},
   'User': {'type': 'keyword'},
   'UserGroup': {'type': 'keyword'},
   'timestamp': {'type': 'date'}}}}

# we need a list of indexes for reindex.
source = Elasticsearch("https://xxx:xxx@es-lhcb.xxxx.cern.ch:9203", timeout = 600, use_ssl = True, verify_certs = True, ca_certs='/home/zmathe/reindex/cas.pem')
res = source.indices.get('lhcb-production_wmshistory_index-*')

indexes = [i for i in res]
indexes.reverse()

def do_reindex(indexName):
  source = Elasticsearch("https://xxx:xxx@es-lhcb.xxx.cern.ch:9203", timeout = 600, use_ssl = True, verify_certs = True, ca_certs='/home/zmathe/reindex/cas.pem')
  destination = Elasticsearch("https://yyy:yyy@es-lhcb-yyy.cern.ch:9203", timeout = 600, use_ssl = True, verify_certs = True, ca_certs='/home/zmathe/reindex/cas.pem')
  if not destination.indices.exists(indexName):
    res = destination.indices.create(indexName, body={'mappings': mapping})
    if not res['acknowledged']:
      print("Problem creating:", indexName)
      return
    print ("Indexing:", indexName)
    res = reindex(source, indexName, indexName, target_client=destination)
    print("Reindexed:", indexName,  multiprocessing.current_process().name, res)

if __name__ == '__main__':
    pool = multiprocessing.Pool(processes=2)
    pool.map(do_reindex, indexes)
