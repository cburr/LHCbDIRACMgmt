FROM cern/cc7-base
RUN yum install -y python2-pip cern-get-sso-cookie
RUN pip install --upgrade pip setuptools
COPY containers /tmp/containers
WORKDIR /tmp/containers
RUN python setup.py install
RUN yum clean all
